﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Btn_Submit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Buton apasat!");
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Ati selectat optiunea 1");
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Ati selectat optiunea 2");
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show(comboBox1.SelectedItem.ToString());
        }
    }
}
