﻿namespace WinFormsApp
{
    partial class FrmAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnA = new System.Windows.Forms.Button();
            this.BtnB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnA
            // 
            this.BtnA.Location = new System.Drawing.Point(71, 96);
            this.BtnA.Name = "BtnA";
            this.BtnA.Size = new System.Drawing.Size(178, 123);
            this.BtnA.TabIndex = 0;
            this.BtnA.Text = "ButonA";
            this.BtnA.UseVisualStyleBackColor = true;
            this.BtnA.Click += new System.EventHandler(this.BtnA_Click);
            // 
            // BtnB
            // 
            this.BtnB.Location = new System.Drawing.Point(426, 101);
            this.BtnB.Name = "BtnB";
            this.BtnB.Size = new System.Drawing.Size(177, 118);
            this.BtnB.TabIndex = 1;
            this.BtnB.Text = "ButonB";
            this.BtnB.UseVisualStyleBackColor = true;
            // 
            // FrmAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 360);
            this.Controls.Add(this.BtnB);
            this.Controls.Add(this.BtnA);
            this.Name = "FrmAbout";
            this.Text = "About";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnA;
        private System.Windows.Forms.Button BtnB;
    }
}