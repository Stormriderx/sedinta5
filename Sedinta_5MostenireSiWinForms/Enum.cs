﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sedinta_5MostenireSiWinForms
{
   public enum TipCombustibil
    {
        Benzina,
        Motorina,
        GPL,
        Kerosen
    }
    public enum TipMasina
    {
        Berlina,
        Break,
        Limuzina
    }
    public enum Producator
    {
        Mercedes,
        Dacia,
        Tesla
    }
    public enum TipMijlocTransportAerian
    {
        Avion,
        Elicopter
    }

}
