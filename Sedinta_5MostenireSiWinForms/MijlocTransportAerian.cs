﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sedinta_5MostenireSiWinForms
{
    class MijlocTransportAerian : MijlocTransportMotorizat
    {
        public TipMijlocTransportAerian Tip { get; set; }

        public int VitezaCroaziera { get; set; }
    }
}
