﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sedinta_5MostenireSiWinForms
{
    class Masina : MijlocTransportMotorizat
    {
        public int NrRoti { get; set; }
        public int NrUsi { get; set; }
        public TipMasina TipCaroserie { get; set; }
        public Producator Marca { get; set; }
        public string Model { get; set; }
    }
}
