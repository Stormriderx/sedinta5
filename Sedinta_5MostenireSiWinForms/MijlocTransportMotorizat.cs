﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sedinta_5MostenireSiWinForms
{
    class MijlocTransportMotorizat : MijlocTransport
    {
        public int NrCaiPutere { get; set; }
        public TipCombustibil Combustibil { get; set; }
    }
}
