﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sedinta_5MostenireSiWinForms
{
    class MasinaElectrica : Masina
    {
        public int DurataIncarcareInMinute { get; set; }
        public bool PosibilitateIncarcareRapida { get; set; }
    }
}
